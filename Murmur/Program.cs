﻿using System;

namespace Murmur
{
    internal class Program
    {
        /*
         * murmur("abcd") -> "A-Bb-Ccc-Dddd"
         * murmur("RqaEzty") -> "R-Qq-Aaa-Eeee-Zzzzz-Tttttt-Yyyyyyy"
         * murmur("cwAt") -> "C-Ww-Aaa-Tttt"
         */
        public static void Main(string[] args)
        {
            Console.WriteLine(Murmur("abcd"));
            Console.ReadKey();
        }

        public static string Murmur(string s)
        {
            string cuvant = "";
            
            for (int i = 0; i < s.Length; i++)
            {
                for (int j = 0; j <= i; j++)
                {
                    if (j == 0)
                        cuvant += Char.ToUpper(s[i]);
                    else
                        cuvant += Char.ToLower(s[i]);
                }

                if (i < s.Length - 1)
                    cuvant += '-';
            }

            return cuvant;
        }
    }
}